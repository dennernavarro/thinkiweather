import {
  dateFormat,
  getIcon,
  kelvinToCelsius,
  msToKmph,
  timeFormat,
  weekday,
} from "./helpers";

describe("helpers.ts", () => {
  it("dateFormat", () => {
    expect(dateFormat("2021-11-07 00:00:00")).toBe("November 7");
  });

  it("weekday", () => {
    expect(weekday("2021-11-07 00:00:00")).toBe("Sunday");
  });

  it("timeFormat", () => {
    expect(timeFormat("2021-11-07 00:00:00")).toBe("00:00");
  });

  it("getIcon", () => {
    expect(getIcon("10d")).toBe("http://openweathermap.org/img/wn/10d@2x.png");
  });

  it("kelvinToCelsius rounded", () => {
    expect(kelvinToCelsius(0)).toBe(-274);
    expect(kelvinToCelsius(301)).toBe(27);
  });

  it("msToKmph", () => {
    expect(msToKmph(1)).toBe(3);
    expect(msToKmph(15)).toBe(54);
    expect(msToKmph(20)).toBe(72);
  });
});
