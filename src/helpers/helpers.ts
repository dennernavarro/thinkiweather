export const dateFormat = (dateTime: string) => {
  const options = {
    month: "long",
    day: "numeric",
  } as const;

  const date = new Date(dateTime);
  return date.toLocaleDateString("en-US", options);
};

export const weekday = (dateTime: string) => {
  const options = {
    weekday: "long",
  } as const;

  const date = new Date(dateTime);
  return date.toLocaleDateString("en-US", options);
};

export const timeFormat = (timeFormat: string) => {
  const time = timeFormat.split(" ")[1];
  const splittedTime = time.split(":");
  return `${splittedTime[0]}:${splittedTime[1]}`;
};

export const getIcon = (icon: string) => {
  return `http://openweathermap.org/img/wn/${icon}@2x.png`;
};

export const kelvinToCelsius = (temperature: number) => {
  return Math.floor(temperature - 273.15);
};

export const msToKmph = (speed: number) => {
  const kmToM = 1000.0;
  const hrToSec = 3600.0;
  return Math.floor((speed / kmToM) * hrToSec);
};
