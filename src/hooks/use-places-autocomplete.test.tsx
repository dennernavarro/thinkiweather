import React from "react";
import { renderHook } from "@testing-library/react-hooks";
import { usePlacesAutocomplete } from "./use-places-autocomplete";
import { predictions } from "../test/mocks/autocomplete.mocks";
import * as GoogleService from "../services/autocomplete";
import { act } from "react-dom/test-utils";

describe("usePlacesAutoComplete", () => {
  let spy: jest.SpyInstance;

  beforeAll(() => {
    // get rid of async
    spy = jest.spyOn(GoogleService, "googleAutocomplete");
  });

  afterEach(() => spy.mockRestore()); // main difference is here

  afterAll(() => {
    jest.restoreAllMocks();
  });

  test("fetchs the predictions", async () => {
    const spy = jest.spyOn(GoogleService, "googleAutocomplete");
    spy.mockReturnValue(
      Promise.resolve(predictions.predictions as unknown as Promise<any>)
    );
    let initialValue = "natal";

    const { result, waitForNextUpdate } = renderHook(() =>
      usePlacesAutocomplete(initialValue)
    );

    await waitForNextUpdate();

    expect(result.current).toBe(predictions.predictions);
  });

  // test("should fail if text is empty", async () => {
  //   spy.mockReturnValue(Promise.reject());
  //   let initialValue = "";

  //   const { result, waitForNextUpdate } = renderHook(() =>
  //     usePlacesAutocomplete(initialValue)
  //   );

  //   await waitForNextUpdate();
  //   expect(result.current).toEqual([]);
  // });
});
