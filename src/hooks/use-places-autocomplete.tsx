import { useState, useEffect } from "react";
import { Predictions } from "../models/google-predictions.models";
import { googleAutocomplete } from "../services/autocomplete";

export function usePlacesAutocomplete(text = "", debounceTimeout = 400) {
  const [predictions, setPredictions] = useState<Array<Predictions>>([]);

  useEffect(() => {
    const handleDebounce = setTimeout(async () => {
      try {
        if (!text) {
          setPredictions([]);
          return [];
        }
        const nextPredictions = await googleAutocomplete(text);
        if(nextPredictions === null) {
          setPredictions([]);
          return
        }
        setPredictions(nextPredictions);
      } catch (e: any) {
        setPredictions([]);
        return [];
      }
    }, debounceTimeout);

    return () => {
      clearTimeout(handleDebounce);
    };
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [text, debounceTimeout]);

  return predictions;
}
