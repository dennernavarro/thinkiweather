export interface Predictions {
  description: string;
  matched_substrings: MatchedSubstrings[];
  place_id: string;
  reference: string;
  structured_formatting: {
    main_text: string;
    main_text_matched_substrings: MatchedSubstrings[];
    secondary_text: string;
  };
  terms: [];
  types: [];
}

export interface MatchedSubstrings {
  length: number;
  offset: number;
}
