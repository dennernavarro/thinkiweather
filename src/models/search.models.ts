export interface ForecastResponse {
  city: {
    coord: { lat: number; lon: number };
    country: string;
    id: number;
    name: string;
    population: number;
    sunrise: number;
    sunset: number;
    timezone: number;
  };
  cnt: number;
  cod: string;
  list: ListForecast[];
}

export interface ListForecast {
  clouds: { all: string };
  dt: number;
  dt_txt: string;
  main: {
    feels_like: number;
    grnd_level: number;
    humidity: number;
    pressure: number;
    sea_level: number;
    temp: number;
    temp_kf: number;
    temp_max: number;
    temp_min: number;
  };
  pop: number;
  weather: WeatherInfo[];
  wind: {
    deg: number;
    gust: number;
    speed: number;
  };
}

export interface WeatherInfo {
  description: string;
  icon: string;
  id: number;
  main: string;
}

export interface WeatherByDate {
  date: string;
  weather: ListForecast[];
}
