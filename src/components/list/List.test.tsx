import React from "react";
import List from "./List";
import { render, screen } from "@testing-library/react";
import { weatherMock } from "../../test/mocks/weather.mocks";
import userEvent from "@testing-library/user-event";

test("renders widget", () => {
  render(<List weather={weatherMock} />);
  const linkElement = screen.getByTestId("test-list");
  expect(linkElement).toBeInTheDocument();
});

test("select time", () => {
  render(<List weather={weatherMock} />);
  const btn = screen.getByTestId("select-time-0")
  userEvent.click(btn);
});
