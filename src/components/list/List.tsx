import React, { useEffect, useState } from "react";
import {
  dateFormat,
  getIcon,
  kelvinToCelsius,
  msToKmph,
  timeFormat,
} from "../../helpers/helpers";
import {
  ForecastResponse,
  ListForecast,
  WeatherByDate,
} from "../../models/search.models";
import Widget from "../widget/Widget";
import "./list.scss";

export interface ListForecastProps {
  weather: ForecastResponse;
}

export default function List({ weather }: ListForecastProps) {
  const [currentDay, setCurrentDay] = useState<ListForecast[] | null>(null);
  const [selectedTime, setSelectedTime] = useState<ListForecast | null>(null);
  const [selectedTimeIndex, setSelectedTimeIndex] = useState<number>(0);
  const weatherByDate = (): WeatherByDate[] => {
    const groups = weather.list.reduce((groups: any, weather: ListForecast) => {
      const date = weather.dt_txt.split(" ")[0];
      if (!groups[date]) {
        groups[date] = [];
      }
      groups[date].push(weather);
      return groups;
    }, {});

    const groupArrays = Object.keys(groups).map((date) => {
      return {
        date,
        weather: groups[date] as ListForecast[],
      };
    });

    return groupArrays;
  };

  useEffect(() => {
    if (currentDay === null || selectedTime === null) {
      const weatherToday = weatherByDate()[0];
      setCurrentDay(weatherToday.weather);
      setSelectedTime(weatherToday.weather[0]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [weather]);

  useEffect(() => {
    if (currentDay) {
      setSelectedTime(currentDay[0]);
      setSelectedTimeIndex(0);
    }
  }, [currentDay]);

  function selectTimeHandler(index: number) {
    if (currentDay) {
      setSelectedTime(currentDay[index]);
      setSelectedTimeIndex(index);
    }
  }

  if (currentDay === null || selectedTime === null) {
    //loading
    return <div data-testid="loading">Loading data...</div>;
  }

  return (
    <div className="list" data-testid="test-list">
      <div className="highlight">
        <h1 className="card-title">{weather.city.name}</h1>
        <div className="summary">
          <span>{dateFormat(selectedTime.dt_txt)} - </span>
          <span id="wob_dc"> {selectedTime.weather[0].description}</span>
        </div>
      </div>

      <div className="weather-body">
        <div className="left-info">
          <div className="weather-icon">
            <img
              src={getIcon(selectedTime.weather[0].icon)}
              alt="weather-icon"
            />
          </div>
          <div className="weather-temperature">
            <span className="temp">
              {kelvinToCelsius(selectedTime.main.temp)}
            </span>{" "}
            <span className="scale">°C</span>
          </div>
        </div>

        <div className="right-info">
          <span>Precip: {selectedTime.pop}%</span>
          <span>Humidity: {selectedTime.main.humidity}%</span>
          <span>Wind: {msToKmph(selectedTime.wind.speed)} km/h</span>
        </div>
      </div>

      <div className="time-selector">
        {currentDay.map((times, index) => (
          <button
            data-testid={`select-time-${index}`}
            onClick={() => selectTimeHandler(index)}
            className={selectedTimeIndex === index ? "active" : undefined}
            key={index}
          >
            {timeFormat(times.dt_txt)}
          </button>
        ))}
      </div>

      <div className="weather-footer">
        {weatherByDate().map((day, index) => {
          return <Widget day={day} setCurrentDay={setCurrentDay} key={index} />;
        })}
      </div>
    </div>
  );
}
