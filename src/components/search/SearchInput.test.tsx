import React from "react";
import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import SearchInput from "./SearchInput";
import { usePlacesAutocomplete } from "../../hooks/use-places-autocomplete";
import { predictions } from "../../test/mocks/autocomplete.mocks";

jest.mock("../../services/autocomplete");

// jest.mock("../../hooks/use-places-autocomplete", () => ({
// //   ...jest.requireActual("../../services/autocomplete"),
//   googleAutocomplete: jest.fn().mockImplementationOnce([]),
// }));

describe("SearchInput", () => {
  const setup = () => {
    const utils = render(<SearchInput setWeather={() => {}} />);
    const input = utils.getByTestId("search-location");
    return {
      input,
      ...utils,
    };
  };

  test("renders widget", () => {
    render(<SearchInput setWeather={() => {}} />);
    const linkElement = screen.getByTestId("form-search");
    expect(linkElement).toBeInTheDocument();
  });

  test("enter city name", () => {
    const { input } = setup();
    fireEvent.change(input, { target: { value: "natal" } });
    expect(input.value).toBe("natal");
  });

//   test("should show list of predictions", async () => {
//     const { input } = setup();
//     fireEvent.change(input, { target: { value: "natal" } });
//     // const spy = jest.spyOn(usePlacesAutocomplete, "predictions");

//     // usePlacesAutocomplete.mockReturnValue(predictions.predictions);

//     await waitFor(() => {
//       expect(screen.getByText("natal")).toBeInTheDocument();
//     });
//   });
});
