import React, { useState } from "react";
import Logo from "../../images/logo.png";
import { ForecastResponse } from "../../models/search.models";
import List from "../list/List";
import "./search.scss";
import SearchInput from "./SearchInput";

export default function Search() {
  const [weather, setWeather] = useState<ForecastResponse>(
    {} as ForecastResponse
  );

  return (
    <div className="search-page">
      <div className="search-header" data-testid="header">
        <img src={Logo} alt="logo-thinkiweather" className="logo" />
        <SearchInput setWeather={setWeather} />
      </div>

      {Object.entries(weather).length > 0 && <List weather={weather} />}
    </div>
  );
}
