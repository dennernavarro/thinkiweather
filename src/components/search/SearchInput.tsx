import React, { useEffect, useState } from "react";
import { usePlacesAutocomplete } from "../../hooks/use-places-autocomplete";
import { getWeatherByCity } from "../../services/weather";
import Loading from "../../images/loading.gif";
import "./search-input.scss";
import { ForecastResponse } from "../../models/search.models";
import { Predictions } from "../../models/google-predictions.models";

export interface SearchInputProps {
  setWeather(weather: ForecastResponse): void;
}

export default function SearchInput({ setWeather }: SearchInputProps) {
  const [searchValue, setSearchValue] = useState<string>("");
  const predictions = usePlacesAutocomplete(searchValue);

  const [selectedPrediction, setSelectedPrediction] =
    useState<Predictions | null>(null);
  const [displayPredictions, setDisplayPredictions] = useState<boolean>(false);
  const [error, setError] = useState("");

  // When selected the item from the city list
  // the handlePrediction will be fired, hide
  // the list and set the selected prediction
  const handlePredictionSelection = (e: any, prediction: Predictions) => {
    e.preventDefault();
    setSelectedPrediction(prediction);
    setDisplayPredictions(false);
  };

  // after selecting the city from the google api
  // we are going to try to fetch the data from
  // openweathermap
  useEffect(() => {
    async function fetchData() {
      try {
        setError("");
        if (selectedPrediction !== null) {
          let response = await getWeatherByCity(selectedPrediction.description);
          setWeather(response.data);
        }
      } catch (e: any) {
        if (e.response.status === 404) {
          setError(e.response.data.message);
        }
      }
    }

    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedPrediction]);

  useEffect(() => {
    if (predictions.length && searchValue.length) {
      setDisplayPredictions(true);
    } else {
      setDisplayPredictions(false);
    }
  }, [searchValue, predictions]);

  return (
    <form
      className="search-form"
      data-testid="form-search"
      onSubmit={(e) => e.preventDefault()}
    >
      <input
        className="search"
        placeholder="Search by location"
        data-testid="search-location"
        type="text"
        value={searchValue}
        onChange={(e) => setSearchValue(e.target.value)}
        autoComplete="off"
      />
      {error.length > 0 && <span className="not-found">{error}</span>}
      <img src={Loading} alt="loading" className="loading" width="300" />

      <ul
        className="search-list"
        style={{ display: displayPredictions ? "flex" : "none" }}
      >
        {predictions.length &&
          predictions?.map((prediction: any) => (
            <li
              key={prediction?.place_id}
              className="search-item-result"
              role="button"
              onClick={(e) => handlePredictionSelection(e, prediction)}
              onKeyDown={(e) => handlePredictionSelection(e, prediction)}
            >
              <span>{prediction?.description || "Not found"}</span>
            </li>
          ))}
      </ul>
    </form>
  );
}
