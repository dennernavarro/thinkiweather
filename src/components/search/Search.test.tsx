import React from "react";
import { render, screen } from "@testing-library/react";
import Search from "./Search";

test("renders widget", () => {
  render(<Search />);
  const linkElement = screen.getByTestId("header");
  expect(linkElement).toBeInTheDocument();
});

test("renders List", () => {
  render(<Search />);
  const linkElement = screen.getByTestId("header");
  expect(linkElement).toBeInTheDocument();
});