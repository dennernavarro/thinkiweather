import React from "react";
import { getIcon, kelvinToCelsius, weekday } from "../../helpers/helpers";
import { ListForecast, WeatherByDate } from "../../models/search.models";
import "./widget.scss";

export interface WidgetProps {
  day: WeatherByDate;
  setCurrentDay(day: ListForecast[]): void;
}

export default function Widget({ day, setCurrentDay }: WidgetProps) {
  const temperatureVariation = () => {
    const max: number[] = [];
    const min: number[] = [];
    day.weather.map((temp) => {
      max.push(temp.main.temp_max);
      min.push(temp.main.temp_min);
    });

    const maxResult = Math.max(...max);
    const minResult = Math.min(...min);
    return { maxResult, minResult };
  };

  return (
    <div
      className="widget-info"
      role="button"
      data-testid="widget"
      onClick={() => setCurrentDay(day.weather)}
      onMouseEnter={() => setCurrentDay(day.weather)}
    >
      <span>{weekday(day.date)}.</span>
      <img src={getIcon(day.weather[0].weather[0].icon)} alt="weather-icon" />
      <span>{kelvinToCelsius(temperatureVariation().maxResult)}°</span>
      <span>{kelvinToCelsius(temperatureVariation().minResult)}°</span>
    </div>
  );
}
