import React from "react";
import { render, screen } from "@testing-library/react";
import Widget from "./Widget";
import userEvent from "@testing-library/user-event";
import { mockWeatherByDate } from "../../test/mocks/weatherByDate.mock";

test("renders widget", () => {
  render(<Widget day={mockWeatherByDate} setCurrentDay={() => {}} />);
  const linkElement = screen.getByTestId("widget");
  expect(linkElement).toBeInTheDocument();
});

test("fire click event", () => {
  const mockCallback = jest.fn();
  render(<Widget day={mockWeatherByDate} setCurrentDay={mockCallback} />);
  userEvent.click(screen.getByTestId("widget"));
  expect(mockCallback).toHaveBeenCalled();
});
