/* eslint-disable no-param-reassign */
import axios from "axios";

const instance = axios.create({
  // .. where we make our configurations
  baseURL: "https://api.openweathermap.org/data/2.5/",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
});

export default instance;
