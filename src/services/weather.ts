import instance from "../api";
import { ForecastResponse } from "../models/search.models";

export function getWeatherByCity(address: string) {
  return instance.get<ForecastResponse>(`forecast?q=${address}&appid=${process.env.REACT_APP_WEATHER_KEY}
  `);
}
