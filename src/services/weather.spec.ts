import instance from "../api";
import { getWeatherByCity } from "./weather";
import { weatherMock } from "../test/mocks/weather.mocks";

jest.mock("../api", () => ({
  get: jest.fn(() => Promise.resolve({ data: {} })),
}));


describe("weather", () => {
  test("should return the weather", async () => {
    instance.get.mockImplementationOnce(() =>
      Promise.resolve({ data: weatherMock })
    );

    const result = await getWeatherByCity("natal, rn");
    expect(result.data).toBe(weatherMock);
  });
});
