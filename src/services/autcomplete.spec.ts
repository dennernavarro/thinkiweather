import { predictions } from "../test/mocks/autocomplete.mocks";
import * as GoogleService from "./autocomplete";

jest.mock("./autocomplete", () => ({
    googleAutocomplete: jest.fn()
}))

describe("autocomplete", () => {

  test("should return list of locations", async () => {
    GoogleService.googleAutocomplete.mockImplementationOnce(() =>
      Promise.resolve({ data: predictions.predictions })
    );

    const result = await GoogleService.googleAutocomplete("natal");
    expect(result.data).toBe(predictions.predictions);
  });

//   test("should reject if text is empty", async () => {
//     GoogleService.googleAutocomplete.mockImplementationOnce(() =>
//       Promise.reject("Need valid text input")
//     );

//     const result = await GoogleService.googleAutocomplete("test");
    
//     expect(result).toBe("Need valid text input");
//   });
});
