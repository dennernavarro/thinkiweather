export function googleAutocomplete(text: string): Promise<any> {
  return new Promise((resolve, reject) => {
    if (!text) {
      reject("Need valid text input");
    }

    try {
      new window.google.maps.places.AutocompleteService().getPlacePredictions(
        { input: text, types: ["geocode"] },
        resolve
      );
    } catch (e) {
      reject(e);
    }
  });
}
