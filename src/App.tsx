import React from "react";
import "./styles/app.scss";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Search from "./components/search/Search";

function App() {
  return (
    <BrowserRouter>
      <div data-testid="app">
        <Routes>
          <Route path="/" element={<Search />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
