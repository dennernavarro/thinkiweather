import React from "react";
import { screen, render } from "@testing-library/react";
import App from "./App";

test("renders widget", () => {
    render(<App />);
    const linkElement = screen.getByTestId('app');
    expect(linkElement).toBeInTheDocument();
  });
