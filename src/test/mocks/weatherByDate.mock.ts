import { ListForecast } from "../../models/search.models";

export const weather = {
  clouds: { all: 78 },
  dt: 1636232400,
  dt_txt: "2021-11-06 21:00:00",
  main: {
    temp: 301.46,
    feels_like: 305.2,
    temp_min: 299.86,
    temp_max: 301.46,
    pressure: 1009,
  },
  pop: 0,
  sys: { pod: "n" },
  visibility: 10000,
  weather: [
    {
      description: "broken clouds",
      icon: "04n",
      id: 803,
      main: "Clouds",
    },
  ],
  wind: { speed: 3.81, deg: 96, gust: 6.1 },
} as unknown as ListForecast;

export const mockWeatherByDate = {
  date: "2021-11-06",
  weather: [weather],
};
