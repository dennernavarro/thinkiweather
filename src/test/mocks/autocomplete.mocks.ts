/* istanbul ignore file */
import { Predictions } from "../../models/google-predictions.models";

export const autocompleteResult = {
  description: "Natal, State of Rio Grande do Norte, Brazil",
  matched_substrings: [
    {
      length: 5,
      offset: 0,
    },
  ],
  place_id: "ChIJMQVGJqyqswcRae0P8ExAi10",
  reference: "ChIJMQVGJqyqswcRae0P8ExAi10",
  structured_formatting: {
    main_text: "Natal",
    main_text_matched_substrings: Array(1),
    secondary_text: "State of Rio Grande do Norte, Brazil",
  },
  terms: [
    { offset: 0, value: "Natal" },
    { offset: 7, value: "State of Rio Grande do Norte" },
    { offset: 37, value: "Brazil" },
  ],
  types: ["locality", "political", "geocode"],
} as unknown as Predictions;

export const predictions = {
  predictions: [autocompleteResult, autocompleteResult, autocompleteResult],
  status: "OK",
};
