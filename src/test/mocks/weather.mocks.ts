import { ForecastResponse } from "../../models/search.models";
import { weather } from "./weatherByDate.mock";

export const weatherMock = {
  city: {
    coord: { lat: -5.8054, lon: -35.2081 },
    country: "BR",
    id: 3394023,
    name: "Natal",
    population: 763043,
    sunrise: 1636185257,
    sunset: 1636229680,
    timezone: -10800,
  },
  cnt: 40,
  cod: "200",
  list: [weather, weather],
  message: 0,
} as unknown as ForecastResponse;
