### Date

November 7th, 2021

### Location of deployed application

[Link to the app](https://thinkiweather.herokuapp.com/)

### Time spent

About 10 of work

### Assumptions made

For the search feature, I have implemented a google search feature that the final user can easily find the city by typing a few words.
As a user, I like to have short information in a card summarized and that's the reason why I've done a small card for mobile/desktop
where we can easily understand the weather of the given city


### Shortcuts/Compromises made

Normally I try to cover at least 95% coverage with unit tests. Unfortunately, I only covered ~83%.
I had a problem mocking and covering the Google Autocomplete service. In this case, due to the time
spent on this project, I won't cover all although I feel it's very important.


### Instructions to run assignment locally

You must have a .env file with the variable:
```
REACT_APP_WEATHER_KEY =  YOUR_KEY_FROM_OPEN_WEATHER_MAP
```

- You should have node & npm installed on your machine
- npm install
- npm start

### What did you not include in your solution that you want us to know about?

My wish was to add e2e testing with CodeceptIo + Playwright but the time was too short to
do the final implementation. As I mentioned in the previous topic, I spent few time doing 
unit tests. 

### Other information about your submission that you feel it's important that we know if applicable.

### Your feedback on this technical challenge
